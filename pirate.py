#! /bin/python3

# GNU GPLv3-or-later

# pip3 install youtubesearchpython
from youtubesearchpython import VideosSearch
import youtube_dl
from sys import argv

def selection(url):

    FFMPEG_OPTIONS = {'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5', 'options': '-vn'}
    with youtube_dl.YoutubeDL({"format":"bestaudio"}) as ydl:
        info = ydl.extract_info(url, download = 0)
        
        toremove=[]
        [toremove.append(i) for i in info["formats"] if i["filesize"] is None]
        url2 = info['formats'][0]['url']
    
    return url2




def search(QUERY):

    videosSearch = VideosSearch(QUERY, limit=50)
    out = videosSearch.result()
    result = out.get('result', {})
    
    titles = []
    links = []
    for i in range(len(result)):
        titles.append( result[i].get('title', 'TITLE UNAVAIABLE') )
        links.append(  result[i].get('link', '')                  )
        # cannel?, more info? there's a lot that could be added

    return titles, links
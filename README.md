# spotifuck

Free And Open Source music streaming/downloading service

## Host an instance

* Clone repo
* Install dependencies `pip3 install -r requirements.txt`
* run `gunicorn main:app`
* enjoy :)
# to run:
# gunicorn main:app

# everything here is under the GNU GPL
# https://www.gnu.org/licenses/gpl-3.0.en.html

from flask import Flask, request, url_for, render_template, redirect, session
import random as r
import json
from glob import glob
import os
import hashlib
from datetime import datetime as dt

import pirate

CONFIG = {"debug": 0, "port": 8080}


def printd(*args):
    if CONFIG["debug"]:
        print(*args)


def reverse(lst):
    return [ele for ele in reversed(lst)]


def DisplayError(error):
    return f'Error: {error}<br/><a href="/">Back to home</a>'


app = Flask(__name__)


@app.route('/css')
def get_fancy():
    with open('templates/base.css', 'r') as f:
        return f.read()


@app.route('/')
def home():
    args = request.args

    if "queue" not in session: session["queue"] = []

    return render_template('index.html', queue=session["queue"], results=[])


@app.route('/search')
def search():
    args = request.args

    if "queue" not in session: session["queue"] = []

    match = args['q'].lower()

    titles, links = pirate.search(match)

    results = [(title, links[i].split('?v=')[1])
               for i, title in enumerate(titles)]

    return render_template('index.html',
                           queue=session["queue"],
                           results=results)


@app.route("/addtoqueue")
def addtoqueue():
    args = request.args

    if "queue" not in session: session["queue"] = []

    session["queue"] += [(args["id"], args["title"])]

    return redirect("/")


@app.route("/loadfromyt")
def loadfromyt():
    args = request.args

    if "queue" not in session: session["queue"] = []

    session["queue"] += [(args["url"].split("?v=")[1], "Imported ("+args["url"]+')')]

    return redirect("/")


@app.route("/remove/<id>")
def removequeue(id):
    print(id)

    if "queue" not in session: session["queue"] = []

    session["queue"] = [i for i in session["queue"] if id not in i]

    return redirect("/")


@app.route('/view')
def view():

    args = request.args

    if "queue" not in session: session["queue"] = []

    if session.get("queue", []) != []:

        link = "https://www.youtube.com/watch?v=" + session["queue"][0][0]
        title = session["queue"][0][1]

        session["queue"] = session["queue"][1:]

    else:
        link = "https://www.youtube.com/watch?v=" + args.get(
            "id", "j8uEGcumHGE")
        title = args.get("title", "This is the end of your queue :)")

    url2 = pirate.selection(link)

    return render_template('watch.html',
                           queue=session["queue"],
                           link=url2,
                           title=title)


@app.route("/export")
def exportqueue():

    if "queue" not in session: session["queue"] = []

    return json.dumps(session["queue"])


@app.route("/import")
def imoprtqueue():

    code = request.args["code"]

    try:
        session["queue"] = json.loads(code)
    except:
        return "Invalid queue code"

    return redirect("/")


@app.route('/issue')
def issue():
    return render_template('issue.html')


@app.route('/issuer', methods=["POST"])
def issuer():

    text = request.form['issue']
    reply = request.form.get('email', '')
    date = dt.now().strftime("%d/%m/%Y")

    with open('issues.txt', 'a') as f:
        f.write(
            f"-------------------------------------------\non {date}\n{text}\nREPLY: {reply}\n"
        )

    return "Thanks <a href='/'>Home</a>"


app.secret_key = os.urandom(12)
app.run(host='0.0.0.0',
        port=CONFIG.get("port", 8080),
        debug=CONFIG.get("debug", 0))
